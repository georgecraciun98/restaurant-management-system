package BusinessLayer;

public interface MenuItem {
    public int computeprice();
    public String toString();
}
