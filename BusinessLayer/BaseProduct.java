package BusinessLayer;

public class BaseProduct implements MenuItem {
   private int price;
   private int id;
   private String name;
   public BaseProduct(int a,int b,String c){
       id=a;
       price=b;
       name=c;
   }
   public int computeprice(){
       return price;
   }

    public String getName() {
        return name;
    }
    public int getId() {
        return id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getPrice() {
        return price;
    }

    public void setprice(int a) {
        this.price = a;
    }
    public String toString(){
      return "id= "+id+" price= "+price+" name= "+name;
    }

}
