package BusinessLayer;

import java.util.ArrayList;
import java.util.List;

public class CompositeProduct implements MenuItem {

    private List<MenuItem> menuItemList=new ArrayList<MenuItem>();

    public int computeprice() {
        int sum=0;
        for(MenuItem x:menuItemList){
            sum+=x.computeprice();
        }
        return sum;
    }
    public void addMenuItem(MenuItem y){
        menuItemList.add(y);
    }
    public void removeMenuItem(MenuItem y){
        menuItemList.remove(y);
    }
    public String toString(){
        String s="";

        for(MenuItem x:menuItemList)
            s+=x.toString();
        return s;
    }
}
