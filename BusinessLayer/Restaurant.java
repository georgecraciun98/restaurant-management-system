package BusinessLayer;

import PresentationLayer.AdministrationGraphicalUserInterface;
import PresentationLayer.ChefGraphicalUserInterface;
import PresentationLayer.WaiterGraphicalUserInterface;

import javax.swing.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Restaurant  {

    private HashMap<Order,List<MenuItem>> hashMenu=new HashMap<Order,List<MenuItem>>();
    List<BaseProduct> restaurantMenu=new ArrayList<BaseProduct>();
    public ChefGraphicalUserInterface chef=new ChefGraphicalUserInterface();
    public  WaiterGraphicalUserInterface waiter=new WaiterGraphicalUserInterface(hashMenu,restaurantMenu);

    JPanel panel =waiter.panel();
    public  AdministrationGraphicalUserInterface admin=new AdministrationGraphicalUserInterface(restaurantMenu,panel,waiter);
   public Restaurant() throws ParseException {
   waiter.addObserver(chef);

   }




}
