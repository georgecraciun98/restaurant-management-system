package BusinessLayer;

import java.util.Date;

public class Order {
    private int OrderID;
    private Date Date;
    private int Table;
    public Order(int a,Date b,int c){
        OrderID=a;
        Date=b;
        Table=c;
    }
    public int getOrderID()
    {
        return OrderID;
    }
    public Date getDate(){
        return Date;
    }
    public int getTable(){
        return Table;
    }
    public int hashCode(){
        return OrderID;
    }
    public String toString(){
        return "id= "+OrderID+" date= "+Date+" table= "+Table;
    }
}
