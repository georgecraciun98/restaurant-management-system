package DataLayer;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class FileWriter {

    public void do_it(String s){

        PrintWriter writer = null;
        try {
            writer = new PrintWriter("Check.txt", "UTF-8");
            writer.println(s);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        writer.close();
    }

}
