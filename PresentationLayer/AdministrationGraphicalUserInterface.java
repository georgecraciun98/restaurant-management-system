package PresentationLayer;

import BusinessLayer.*;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class AdministrationGraphicalUserInterface implements RestaurantProcessing {

    List<BaseProduct> restaurantMenu;
   public  JButton button1=new JButton("New Menu Item");
    public  JButton button2=new JButton("Delete Menu Item");
    public  JButton button3=new JButton("Edit Menu Item");
 WaiterGraphicalUserInterface waiter;

    public JButton showAll=new JButton("Show menu");
    public JTextField id=new JTextField(20);
    public JTextField price=new JTextField(20);
    public JTextField name=new JTextField(20);
    public JTextField text1=new JTextField("id");
    public JTextField text2=new JTextField("price");
    public JTextField text3=new JTextField("name");
    public JPanel p2;
    public JTable tabel;
    BaseProduct b1=new BaseProduct(1,10,"Felul intai");
    BaseProduct b2=new BaseProduct(2,12,"Ciorba");
    BaseProduct b3=new BaseProduct(3,15,"Gratar cu cartofi prajiti");
    BaseProduct b4=new BaseProduct(4,25,"Paste Carbonara");

    JScrollPane scroll;

    GridLayout grid=new GridLayout(4,3,8,6);
    JPanel panel=new JPanel();

    public JTable init(){

        String[] columns={"Id","Price","Name"};
        DefaultTableModel first1=new DefaultTableModel(columns,restaurantMenu.size());
        JTable first=new JTable();
        first.setModel(first1);
        first.setRowSelectionAllowed(true);
        first.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        for (int i = 0; i < restaurantMenu.size(); i++){
            first.setValueAt(restaurantMenu.get(i).getId(), i, 0);
            first.setValueAt(restaurantMenu.get(i).getPrice(), i, 1);
            first.setValueAt(restaurantMenu.get(i).getName(), i, 2);

        }
    return first;
    }
    public AdministrationGraphicalUserInterface(List<BaseProduct> x,JPanel p3,WaiterGraphicalUserInterface waiter1){
        waiter=waiter1;
        p2=p3;

        restaurantMenu=x;
        restaurantMenu.add(b1);
        restaurantMenu.add(b2);
        restaurantMenu.add(b3);
        restaurantMenu.add(b4);


        JFrame frame=new JFrame("Admin user interface");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(350, 350);
        tabel=init();


        Font font1 = new Font("SansSerif", Font.BOLD, 20);
        panel.setLayout(grid);
        button1.setFont(font1);
        button2.setFont(font1);
        button3.setFont(font1);
        showAll.setFont(font1);
        text1.setFont(font1);
        text2.setFont(font1);
        text3.setFont(font1);

        text1.setEditable(false);
        text2.setEditable(false);
        text3.setEditable(false);

        scroll=new JScrollPane(tabel);
        button1.addActionListener(new NewMenuItem());
        button2.addActionListener(new DeleteMenuItem());
        button3.addActionListener(new EditMenuItem());


        showAll.addActionListener(e -> {
         System.out.println("The complet menu is shown above ");
         for(BaseProduct x1:restaurantMenu){
             System.out.println(x1);
         }

        });
        id.setFont(font1);
        price.setFont(font1);
        name.setFont(font1);
        panel.add(button1);
        panel.add(text1);
        panel.add(id);
        panel.add(button2);
        panel.add(text2);
        panel.add(price);
        panel.add(button3);
        panel.add(text3);
        panel.add(name);
        panel.add(showAll);

        panel.add(scroll);

        waiter.refresh(restaurantMenu);
        frame.add(panel);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }






    private class NewMenuItem implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            String price1=price.getText();
            String id1=id.getText();
            String name1=name.getText();
            try{

                BaseProduct a=new BaseProduct(Integer.parseInt(id1),Integer.parseInt(price1),name1);
                assert a.getPrice()>0 :"Price must be greater than 0";
                int size=restaurantMenu.size();
                restaurantMenu.add(a);
                assert size!=restaurantMenu.size() : " RestaurantMenu size didn't change";

                tabel=init();
                System.out.println("New item was created "+a);
                panel.remove(scroll);
                scroll=new JScrollPane(tabel);
                panel.add(scroll);
                panel.revalidate();
                waiter.refresh(restaurantMenu);

            }
            catch(Exception e1){
                System.out.println("Wrong values "+e1.getMessage());

            }
        }
    }
    private class DeleteMenuItem implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            try{
                int select=tabel.getSelectedRow();
                BaseProduct base=new BaseProduct((int) tabel.getValueAt(select, 0),
                        (int) tabel.getValueAt(select, 1),(String)tabel.getValueAt(select, 2));
                restaurantMenu.remove(select);
                System.out.println("Item was removed"+base);
                tabel=init();
                panel.remove(scroll);
                scroll=new JScrollPane(tabel);
                panel.add(scroll);
                panel.revalidate();
                waiter.refresh(restaurantMenu);}
            catch(Exception e1){
                System.out.println(e1.getMessage());
            }

        }

    }
    private class EditMenuItem implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            try{
                String price1=price.getText();
                String name1=name.getText();
                int select=tabel.getSelectedRow();
                BaseProduct base=new BaseProduct((int) tabel.getValueAt(select, 0),
                        (int) tabel.getValueAt(select, 1),(String)tabel.getValueAt(select, 2));
                assert  name1!=null : "Name must be different than null ! ";
                base.setName(name1);
                base.setprice(Integer.parseInt(price1));
                int size=restaurantMenu.size();
                restaurantMenu.remove(select);
                restaurantMenu.add(base);
                assert restaurantMenu.size()==size :"Size was changed in Restaurantmenu ! ";
                System.out.println("Item was modified"+base+" Succesfully");
                tabel=init();
                panel.remove(scroll);
                scroll=new JScrollPane(tabel);
                panel.add(scroll);
                panel.revalidate();
                waiter.refresh(restaurantMenu);}
            catch(Exception e1){
                System.out.println("Wrong values "+e1.getMessage());

            }
        }
    }

}
