package PresentationLayer;

import BusinessLayer.BaseProduct;
import BusinessLayer.MenuItem;
import BusinessLayer.Order;
import BusinessLayer.RestaurantProcessing;
import DataLayer.FileWriter;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

public class WaiterGraphicalUserInterface extends Observable implements RestaurantProcessing {
    private HashMap<Order, List<MenuItem>> hashMenu;
    List<BaseProduct> restaurantMenu;
    List<Order> Orders=new ArrayList<>();
    JScrollPane scroll1;
    JScrollPane scroll;
    public JTable tabel;
    public JTable tabel2;
    GridLayout grid = new GridLayout(4, 3, 8, 6);
    public JButton button4 = new JButton("Create new Order");
    public JButton button5 = new JButton("Compute price ");
    public JButton button6 = new JButton("Add item to order");
    public JButton button7=new JButton("CreateBill");

    public JTextField text6 = new JTextField("OrderId");
    public JTextField text7 = new JTextField("Date");
    public JTextField text8 = new JTextField("Table");
    public JTextField orderId =new JTextField(20);
    public JTextField date =new JTextField(20);
    public JTextField table =new JTextField(20);
    public JPanel panel1 = new JPanel();
    public JFrame frame1 = new JFrame("Waiter user interface");
    Order x1=new Order(1,new SimpleDateFormat("dd/MM/yyyy").parse("22/03/1998"),21);


    public WaiterGraphicalUserInterface(HashMap<Order, List<MenuItem>> h, List<BaseProduct> l) throws ParseException {
        hashMenu = h;
        restaurantMenu = l;

        Font font1 = new Font("SansSerif", Font.BOLD, 20);
        panel1.setLayout(grid);
        text6.setEditable(false);
        text7.setEditable(false);
        text8.setEditable(false);
        tabel=init();
        tabel2=init1();
        scroll=new JScrollPane(tabel);
        scroll1=new JScrollPane(tabel2);

        button4.addActionListener(new CreateOrder());
        button6.addActionListener(new AddItem());
        button5.addActionListener(new ComputePrice());
        button7.addActionListener(new CreateBill());
        text6.setFont(font1);
        text7.setFont(font1);
        text8.setFont(font1);
        button4.setFont(font1);
        button5.setFont(font1);
        button6.setFont(font1);

        panel1.add(button4);
        panel1.add(text6);
        panel1.add(orderId);

        panel1.add(button5);
        panel1.add(text7);
        panel1.add(date);

        panel1.add(button6);

        panel1.add(text8);
        panel1.add(table);
        panel1.add(button7);
        panel1.add(scroll);

        panel1.add(scroll1);

        frame1.add(panel1);

        frame1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame1.pack();
        frame1.setVisible(true);

    }

    public JTable init(){

        String[] columns={"Id","Price","Name"};
        DefaultTableModel first1=new DefaultTableModel(columns,restaurantMenu.size());
        JTable first=new JTable();
        first.setModel(first1);
        first.setRowSelectionAllowed(true);
        first.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        for (int i = 0; i < restaurantMenu.size(); i++){
            first.setValueAt(restaurantMenu.get(i).getId(), i, 0);
            first.setValueAt(restaurantMenu.get(i).getPrice(), i, 1);
            first.setValueAt(restaurantMenu.get(i).getName(), i, 2);

        }
        return first;
    }
    public JTable init1(){

        String[] columns={"Id","Date","Table"};
        DefaultTableModel first1=new DefaultTableModel(columns,Orders.size());
        JTable first=new JTable();
        first.setModel(first1);
        first.setRowSelectionAllowed(true);
        first.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        for (int i = 0; i < Orders.size(); i++){
            first.setValueAt(Orders.get(i).getOrderID(), i, 0);
            first.setValueAt(Orders.get(i).getDate(), i, 1);
            first.setValueAt(Orders.get(i).getTable(), i, 2);

        }
        return first;
    }


    public JPanel panel() {
        return panel1;
    }



    private class CreateOrder implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            String id1=orderId.getText();
            String date1= date.getText();
            String table1=table.getText();
            try{

                Order order=new Order(Integer.parseInt(id1),new SimpleDateFormat("dd/MM/yyyy").parse(date1),Integer.parseInt(table1));
                assert order.getDate() !=null : "Date must not be empty!";
                hashMenu.put(order,null);
                int size=Orders.size();
                Orders.add(order);
                assert size!=Orders.size():"Item was not added";
                panel1.remove(scroll1);
                tabel2=init1();
                scroll1=new JScrollPane(tabel2);
                panel1.add(scroll1);
                panel1.revalidate();
                System.out.println("Order "+order+"Was inserted");
            }
            catch(Exception e1){
                System.out.println("Try other values "+e1.getMessage());

            }



        }
    }
    private class CreateBill implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            int selectOrder = tabel2.getSelectedRow();


            try{
                Order order=Orders.get(selectOrder);
                String s="";
                int sum=0;
                for(MenuItem x1:hashMenu.get(Orders.get(selectOrder)) ){
                   s+=x1.toString();
                   s+="\n";
                   sum+=x1.computeprice();
                }
                s+="Total price is "+sum;
                new FileWriter().do_it(s);
            }
            catch(Exception e1){
                System.out.println("Something is missing "+e1.getMessage());

            }



        }
    }
    private class AddItem implements ActionListener {

        public void actionPerformed(ActionEvent e) {
          try {
              int selectOrder = tabel2.getSelectedRow();
              int[] selectedItems = tabel.getSelectedRows();
              List<MenuItem> tempList = new ArrayList<MenuItem>();
              String s="We are cooking \n";
              Order order=Orders.get(selectOrder);
              for (int i : selectedItems) {

                  BaseProduct base = new BaseProduct((int) tabel.getValueAt(i, 0),
                          (int) tabel.getValueAt(i, 1), (String) tabel.getValueAt(i, 2));
                  tempList.add(base);
                  s+=base.toString();
              }
              hashMenu.put(order, tempList);
              for(MenuItem x1:hashMenu.get(Orders.get(selectOrder)) ){
                  System.out.println((BaseProduct)x1);
              }
              System.out.println("Items were inserted");

              setChanged();
              notifyObservers(s);
          }
          catch(Exception e1){
              System.out.println(e1.getMessage());
          }


        }
    }

    private class ComputePrice implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            try {

                int selectOrder = tabel2.getSelectedRow();
                System.out.println(tabel2.getValueAt(selectOrder,0));
                int sum=0;
                for(MenuItem x1:hashMenu.get(Orders.get(selectOrder)) ){
                    System.out.println((BaseProduct)x1);
                    assert x1.computeprice()>0 :"Price must be greater than 0";
                    sum+=x1.computeprice();
                }
               System.out.println(sum);
                assert sum>0:"Total sum must be greater than 0";
            }
            catch(Exception e1){
                System.out.println(e1.getMessage());
            }


        }
    }

    public void refresh( List<BaseProduct> restaurant){
        restaurantMenu=restaurant;
        tabel=init();
        panel1.remove(scroll);
        scroll=new JScrollPane(tabel);
        panel1.add(scroll);
        panel1.revalidate();

    }


}
