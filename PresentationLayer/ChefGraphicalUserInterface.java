package PresentationLayer;

import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

public class ChefGraphicalUserInterface implements Observer {

    private JFrame myFrame=new JFrame("Chef Graphical UI");

    private JTextArea text1=new JTextArea();



    public ChefGraphicalUserInterface(){


        myFrame.add(text1);
        myFrame.setVisible(true);
        myFrame.setSize(200,400);
        myFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    }
    @Override
    public void update(Observable o, Object arg) {
    text1.append((String)arg);
    myFrame.revalidate();
    }
}
